# aptitude

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with aptitude](#setup)
    * [Beginning with aptitude](#beginning-with-aptitude)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Installs aptitude

## Setup

### Beginning with aptitude

See [Usage](#usage)

## Usage

	class { 'aptitude': }

## Reference

### Public Classes

* aptitude: Installs aptitude

## Limitations

This module has been built on and tested against Puppet 4.

The module has beent tested on Debian Jessie.

## Development

Feel free to open issues or submit merge requests.

