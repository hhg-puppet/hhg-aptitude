# == Class: aptitude
#
# See README.md for details
#
# === Authors
#
# Sebastian Rettenberger <rettenberger.sebastian@arcor.de>
#
# === Copyright
#
# Copyright 2016 Sebastian Rettenberger
#
class aptitude {

	package { 'aptitude':
		ensure => 'installed'
	}

}
